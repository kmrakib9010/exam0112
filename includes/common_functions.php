<?php
require_once (dirname(__FILE__)."/config.php");

function dbConnect(){
    $dsn = 'mysql:dbname='.DB_NAME.';host='.DB_HOST.';port='.DB_PORT;
    try {
        $dbh = new PDO($dsn, DB_USER, DB_PASS);
        $dbh->exec("set names utf8");
        return $dbh;
    } catch (PDOException $e) {
        echo $msg = 'DB connection failed: ' . $e->getMessage();
        log_msg($e);
        exit;
    }
}



function getData(){
    try{
        return simplexml_load_file(RSS_URL);
    }
    catch (HttpException $e){
        log_msg($e);
        exit;
    }

}


function getPreparedData(){
    $streamData = getData();

    if(empty($streamData)){
        return array();
    }


    $preparedData = array();
    foreach ($streamData->item as $k=>$item)
    {

        $rss_data = array_merge((array)$item->children() , (array)$item->children("dc", true));

        // Now Match Pattern http://(username).blog(server name).fc2.com/blog-entry-(entry number).html
        $matches = array();
        preg_match('/^(http(s)?:\/\/([^\.]+)\.blog(\d+)?\.fc2\.com)?\/blog-entry-(\d+)\.html/', $rss_data['link'], $matches);

        if(!empty($matches[5])) {
            $rss_data['entry_number'] = $matches[5];
            $rss_data['user_name'] =$matches[3];
            $rss_data['server_number'] = $matches[4];
        }
        else{


//            Links of RSS feed items are not  same.
//            According to requirement links format is   http://(username).blog(server name).fc2.com/blog-entry-(entry number).html
//            Nothing said about other formats (http://freemasonry.jp/blog-entry-1075.html)
//            Taking all RSS feed items which has entry id
            $matches = array();
            preg_match('/\/blog-entry-(\d+)\.html/', $rss_data['link'], $matches);
            if(!empty($matches[1])) {
                $rss_data['entry_number'] = $matches[1];
                $rss_data['user_name'] ="";
                $rss_data['server_number'] = "";
            }
            else {
                // Ignor all others
                continue;
            }

        }
        $preparedData[] = $rss_data;
    }
    return $preparedData;
}



function saveNewData(){
    $dbh = dbConnect();
    $streamData = getPreparedData();

    $sql = "INSERT INTO ".TEST_TABLE." SET 
              entry_number=:entry_number,user_name=:user_name,server_number=:server_number,
              link=:link,title=:title,description=:description,subject=:subject,date=:date,
              creator=:creator,publisher=:publisher";

    $sth = $dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    foreach ($streamData as $data){
        $tmp = array(
                ":entry_number" => $data["entry_number"],
                ":user_name" => $data["user_name"],
                ":server_number" => $data["server_number"],
                ":link" => $data["link"],
                ":title" => $data["title"],
                ":description" => $data["description"],
                ":subject" => $data["subject"],
                ":creator" => $data["creator"],
                ":publisher" => $data["publisher"],
                ":date" => $data["date"],
        );

        try{
            $sth->execute($tmp);
        }
        catch(Exception $e){
            log_msg($e);
        }

    }
}

//Requiremnt : The data in a database need to be deleted automatically after two weeks.
//                i) I assumed, delete data if rss feed date is 2 week old.
function deleteOldData($days=14){
    $dbh = dbConnect();
    $sql = "Delete FROM ".TEST_TABLE."  WHERE date <= :expireDate";
    $sth = $dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $tmp = array(
            ":expireDate" => date('Y-m-d H:i:s',strtotime('-'.$days.'days'))
    );
    $sth->execute($tmp);
}


function log_msg($msg="", $type=""){
    // do some thing
    if(empty($type)){
        $type="debug";
    }
    if(is_array($msg) || is_object($msg)){
        $msg = print_r((array)$msg, true);
    }
    $msg = date('Y-m-d H:i:s')."  :  ".(string)$msg . "\r\n";
    file_put_contents(TMP."/".$type.".log",$msg,FILE_APPEND);


}



// For debug
function pr($v=""){ ?>

<?php
    echo "<pre>";
    if(is_array($v)){
        print_r($v);
    }
    else{
        var_dump($v);
    }
    echo "</pre>";
}


?>