CREATE TABLE `test_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_number` int(11) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `server_number` int(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `description` text,
  `link` varchar(255) DEFAULT NULL,
  `creator` varchar(100) DEFAULT NULL,
  `publisher` varchar(100) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entry_number` (`entry_number`),
  KEY `date` (`date`),
  KEY `server_number` (`server_number`),
  KEY `user_name` (`user_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;