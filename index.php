<?php
require_once (dirname(__FILE__)."/includes/common_functions.php");
require_once (ROOT_DIR."/smarty-3.1.30/libs/Smarty.class.php");



class FC2_Blog extends Smarty {

    private $db = false;
    private $query_param = array();
    private $pagerCount = 10;
    private $data_per_page = 10;
    function __construct()
    {

        // Class Constructor.
        // These automatically get set with each new instance.

        parent::__construct();
        $this->db = dbConnect();

        // Settings for smarty
        $this->setConfigDir(ROOT_DIR."/view/configs/");
        $this->setTemplateDir(ROOT_DIR."/view/templates/");
        $this->setCompileDir(TMP."/view/templates_c/");
        $this->setCacheDir(TMP."/view/cache/");
        $this->debugging = false;
        $this->caching = false;
        $this->cache_lifetime = 120;



    }

    public function renderView(){
        if(!empty($_COOKIE['search'])) {
            $search = json_decode($_COOKIE['search'],true);
            $search = json_decode($_COOKIE['seadata_per_pagerch'],true);
            unset($_SESSION['search']);
            unset($_SESSION['data_per_page']);
        }

        if(!empty($_POST)){
            setcookie("search", '', time() - 3600);
        }
        $fc2data = $this->getData();
        $this->assign("fc2data",$fc2data);
        $this->assign("pagerParam", $this->getPagerParam($this->getTotalPage(), $this->getCurrentPageNumber()));
        $operator_set1 = array('Contain' => 'Contain', 'Start' => 'Begin with', 'End' => 'End with','='=>"Equal to");
        $this->assign('operator_set1', $operator_set1);
        $operator_set2 = array('=' => "=", ">" => ">", "<" => "<", ">=" => ">=", "<=" => "<=", "!=" => "!=");
//        $operator_set2 = array('=' => "Equal to", ">" => "Greater than", "<" => "Less than", ">=" => "Greater or equal to", "<=" => "Less or equal to", "!=" => "Not equal to");
        $this->assign('operator_set2', $operator_set2);
        $this->assign('per_page_arr', array(5,10,20,50,100,500));
        $this->assign('data_per_page', $this->getDataPerPage());

        $this->display('index.html');
    }

    private function getData(){
        $this->query_param = array();
        $sql = "SELECT * FROM ".TEST_TABLE." ". $this->getSearchCondition() . " ". $this->getOderBy() . " " . $this->getLimit();
        $sth = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute($this->query_param);
        $fc2data = array();
        while ($arr = $sth->fetch(PDO::FETCH_ASSOC)) {
            $fc2data[]=$arr;
        }
        return $fc2data;
    }
    private function getSearchCondition(){

        if(!empty($_COOKIE['search'])) {
            $search = json_decode($_COOKIE['search'],true);
        }
        else if(!empty($_SESSION['search'])){
            $search = $_SESSION['search'];
        }

        if(!empty($_REQUEST['search'])){
            $search = $_REQUEST['search'];
        }


        if(empty($search) || !is_array($search)){
            $po = array('operator'=>'','value'=>'');
            $search = array(
                'user_name'=>$po,
                'server_number'=>$po,
                'entry_number'=>$po,
                'date'=>$po
            );
        }
        $_SESSION['search'] = $search;
        setcookie('search', json_encode($search), time() + (60*60*24*365) , "/",$_SERVER['HTTP_HOST']);
        $this->assign('searchParams', $search);


        $search1 = array();
        foreach($search as $k=>$condition){

            if(!empty($condition['operator']) && !empty($condition['value'])){

                $condition['operator'] = strtoupper(trim($condition['operator']));
                $condition['value'] = trim($condition['value']);
                if(in_array($condition['operator'],array("CONTAIN","START","END","=",">","<",">=","<=","!="))) {

                    if($condition['operator']=="CONTAIN"){
                        $condition['operator'] = "LIKE";
                        $condition['value'] = "%".$condition['value']."%";
                    }
                    else if($condition['operator']=="START"){
                        $condition['operator'] = "LIKE";
                        $condition['value'] = $condition['value']."%";
                    }
                    else if($condition['operator']=="END"){
                        $condition['operator'] = "LIKE";
                        $condition['value'] = "%".$condition['value'];
                    }

                    $k = trim($k);
                    if($k=="date"){
                        $search1[] = "DATE_FORMAT(".$k.",'%Y-%m-%d') ".$condition['operator']." :".$k;
                    }
                    else{
                        $search1[] = $k." ".$condition['operator']." :".$k;
                    }
                    $this->query_param[":".$k] = $condition['value'];
                }
            }
        }
        $search1 = implode(" AND ",$search1);

        if(!empty($search1)){
            $search1 = " WHERE ".$search1;
        }


        return $search1;
    }
    private function getOderBy(){
        $order_by = " ORDER BY `date` DESC ";
        return $order_by;
    }
    private function getLimit(){
        $data_per_page = $this->getDataPerPage();
        $page = $this->getCurrentPageNumber();
        $limit = " LIMIT ".($data_per_page*($page-1)).", ".$data_per_page;
        return $limit;
    }




    private function getTotalRecordCount(){
        $sql = "SELECT count(*) FROM ".TEST_TABLE;

        $this->query_param = array();
        $sql = "SELECT count(*) as count FROM ".TEST_TABLE." ". $this->getSearchCondition();
        $sth = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute($this->query_param);
        $res = $sth->fetch(PDO::FETCH_ASSOC);
        if(empty($res['count'])){
            return 0;
        }
        return $res['count'];
    }
    function getTotalPage(){
        $total_records = $this->getTotalRecordCount();
        $data_per_page = $this->getDataPerPage();
        return (int)($total_records/$data_per_page);
    }
    function getCurrentPageNumber(){
        $page = 1;
        if(!empty($_REQUEST['page'])){
            $page = (int)$_REQUEST['page'];
        }
        if($page<=0){
            $page = 1;
        }
        return $page;
    }
    private function getDataPerPage(){

        if(!empty($_COOKIE['data_per_page'])) {
            $data_per_page = (int)$_COOKIE['data_per_page'];
        }
        else if(!empty($_SESSION['data_per_page'])){
            $data_per_page = (int)$_SESSION['data_per_page'];
        }

        if(!empty($_REQUEST['limit'])){
            $data_per_page = (int)$_REQUEST['limit'];
        }

        if(empty($data_per_page) || $data_per_page<=0){
            $data_per_page = $this->data_per_page;
        }
        $_SESSION['data_per_page'] = $data_per_page;
        setcookie('data_per_page', $data_per_page, time() + (60*60*24*365) , "/");
        return $data_per_page;
    }

    function getPageUrl($page=1){
        $data = $_GET;
        $data['page'] = $page;
        return $_SERVER['SCRIPT_NAME']."?".http_build_query($data);

    }

    private function getPagerParam($totalPage,$currentPage){

        if($totalPage < 2){
            return array();
        }

        if($totalPage < $currentPage){
            $currentPage = $totalPage;
        }


        $pagerArr = array();
        $this->pagerCount = 10;

        if($this->pagerCount>=$totalPage){
            for($i=1;$i<=$this->pagerCount && $i<=$totalPage;$i++){
                array_push($pagerArr,array(
                    "title"=>$i,
                    "url"=>$this->getPageUrl($i),
                    "page"=>$i,
                    'number'=>true
                ));
            }
        }
        else if($this->pagerCount>=$currentPage+2){
            for($i=1;$i<=$this->pagerCount-2;$i++){
                array_push($pagerArr,array(
                    "title"=>$i,
                    "url"=>$this->getPageUrl($i),
                    "page"=>$i,
                    'number'=>true
                ));
            }
            array_push($pagerArr,array(
                "title"=>"...",
                "url"=>$this->getPageUrl($this->pagerCount-1),
                "page"=>$this->pagerCount-1,
                'number'=>true
            ));

            array_push($pagerArr,array(
                "title"=>$totalPage,
                "url"=>$this->getPageUrl($totalPage),
                "page"=>$totalPage
            ));

        }
        else if($currentPage>$totalPage-$this->pagerCount+2){
            array_push($pagerArr,array(
                "title"=>"1",
                "url"=>$this->getPageUrl(1),
                "page"=>1,
                'number'=>true
            ));
            array_push($pagerArr,array(
                "title"=>"...",
                "url"=>$this->getPageUrl($totalPage-$this->pagerCount+2),
                "page"=>$totalPage-$this->pagerCount+2,
                'number'=>true
            ));
            for($i=$totalPage-$this->pagerCount+3;$i<=$totalPage;$i++){
                array_push($pagerArr,array(
                    "title"=>$i,
                    "url"=>$this->getPageUrl($i),
                    "page"=>$i,
                    'number'=>true
                ));
            }

        }
        else{

            array_push($pagerArr,array(
                "title"=>"1",
                "url"=>$this->getPageUrl(1),
                "page"=>1,
                'number'=>true
            ));

            $c = ($this->pagerCount-2);
//            $i = $currentPage - (int)($c/2) ;
            $i = $currentPage - 2 ;


            for($ii=1;$ii<=$c;$ii++){
                $title = $i;
                if($ii==1 || $ii==$c){
                    $title = "...";
                }
                array_push($pagerArr,array(
                    "title"=>$title,
                    "url"=>$this->getPageUrl($i),
                    "page"=>$i,
                    'number'=>true
                ));

                $i++;
            }


            array_push($pagerArr,array(
                "title"=>$totalPage,
                "url"=>$this->getPageUrl($totalPage),
                "page"=>$totalPage
            ));
        }

        if($currentPage>1){
            array_unshift($pagerArr,array(
                "title"=>"&lt;&lt;Prev",
                "url"=>$this->getPageUrl($currentPage-1),
                "page"=>$currentPage-1
            ));
            array_unshift($pagerArr,array(
                "title"=>"First",
                "url"=>$this->getPageUrl(1),
                "page"=>1
            ));
        }
        if($currentPage<$totalPage){
            array_push($pagerArr,array(
                "title"=>"Next&gt;&gt;",
                "url"=>$this->getPageUrl($currentPage+1),
                "page"=>$currentPage+1
            ));
            array_push($pagerArr,array(
                "title"=>"Last",
                "url"=>$this->getPageUrl($totalPage),
                "page"=>$totalPage
            ));
        }

        return array('totalPage'=>$totalPage, 'currentPage'=>$currentPage,'data'=>$pagerArr);
    }

}

$fc2_blog = new FC2_Blog();
$fc2_blog->renderView();